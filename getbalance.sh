BAL=$(curl --request GET --header 'Content-Type: application/json' --header 'Accept: application/json' --user $SIPUSER:$SIPPWD "https://api.sipgate.com/v2/balance" -s)
AMOUNT=$(echo $BAL | jq '.amount')
CUR=$(echo $BAL | jq '.currency' | sed -e 's:"::g' )
AMOUNT=$(echo "scale=2; $AMOUNT/10000" | bc -l)
echo "Your current balance is "$CUR $AMOUNT
