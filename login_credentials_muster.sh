# Add your sipgate token-ID and token here:
# TOKENID is an alias for SIPUSER
# TOKEN is an alias for SIPPWD,
# so for legacy reasons you can use those variables as well.
# Take care: The SIP* variables MUST be empty/unset or contain the config to use.
# If they are set and not empty, the corresponding TOKEN* variable is ignored.
TOKENID=
TOKEN=
#uncomment if you use Letterheads Address database
#LETTERHEAD_PATH=
#Add your default CallerID here:
DEFAULT_CALLER_ID="anonymous"
LETTERHEAD_PATH="/home/bernhardt/Entwicklung/Letterhead"
