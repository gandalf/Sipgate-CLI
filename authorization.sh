BASEPATH=$(dirname $(realpath $0))
. $BASEPATH/.env

if [ $TOKENID ] &&  [ -z $SIPUSER ]
then SIPUSER=$TOKENID
fi
if [ $TOKEN ] &&  [ -z $SIPPWD ]
then SIPPWD=$TOKEN
fi

AUTH_TOKEN=$(curl -o /dev/null -s -w "%{http_code}\n" --request GET  --header 'Accept: application/json' --user $SIPUSER:$SIPPWD https://api.sipgate.com/v2/authorization/userinfo -s) 
if [ $AUTH_TOKEN = "200" ];
then
	echo "Login OK"
else
	echo "Login failed"
fi
