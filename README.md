# Sipgate CLI
This is a small hack to send faxes via the sipgate REST API from the command line.

## Deployment
### Dependencies
* `bash >= 4.3`
* `curl`
* `sed`
* `bc`
* `getopt`
* [`jq`](https://stedolan.github.io/jq/download/).
* optional: `ghostscript`

### Configuration
Add your login credentials to `login_credentials_muster.sh` and rename the file to `.env`

Since sipgate enforces 2FA, your usual username and password won't work anymore.
Now, you need a so-called [Personal Access Token](https://app.sipgate.com/w0/personal-access-token), that you can retrieve after you login to the web interface.
Enable at least the following scopes:    
* `balance:read`
* `devices:callerid:read`
* `devices:callerid:write`
* `history:read`
* `sessions:fax:write`

Enter the token-ID as `SIPUSER` or `TOKENID` and the token as `SIPPWD` or `TOKEN`.
It's prefered to use the `TOKEN*` variables, but in that case, the `SIP*` variables **MUST** be left empty/unset.

If you use Letterheads Address database, uncomment the last line of that file and add `your/path/to/Letterhead` 

## Usage
`./fax.sh <letter.pdf> <recipient>`
	with a german fax number `<recipient>`

`./fax.sh <letter.pdf> -handle <handle>`
	asks Letterhead's address database for <handle>'s fax number.

` ./fax.sh -help`
	prints usage Message and exits

sipgate doesn't accept pdf files larger than 30 pages or 10MB.

~~For now, `<recipient>` has to be numbers-only, starting with `+49`. It is planned to parse different number formats.~~ Should be done.
~~Spaces in filename seem to make trouble at the moment - avoid them.~~ Spaces in filenames are difficult. No guarantee, but should be handled now.

`status.sh` shows ID, Date and Status of the last ten fax set from this device.

`balance.sh` just gives your account balance.

`faxreport.sh [options] [id]` loads the report of a given fax id (as shown by status.sh). If no `id` ist given, it takes the last fax sent from this machine.

Options:
* `-p` to only print and not save the report
* `-d` to delete the given id from `.fax_history`

## Recommendation
Put 
```
alias fax="/your/path/to/this/project/fax.sh"
alias faxstatus="/your/path/to/this/project/status.sh"
alias faxbalance="/your/path/to/this/project/balance.sh"
# The following is obsolete, but for documentation/compatibility reasons:
fax-from-handle () { fax $1 "$(/your/path/to/this/project/Letterhead/Adressen/address.sh -f $2)"; }
```
into your `.bashrc` or `.bash_aliases`

