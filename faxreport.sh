#! /bin/bash
# depends on bash >4.3

# Prepare Input Variables
PF=false #print flag
DF=false #delete flag
ID=""    # fax id
IDF=false # ID set flag
BASEPATH=$(dirname $(realpath $0))

for ARG in $@
do
	case $ARG in
		"-p") PF=true ;;
		"-d") DF=true ;;
		"-pd") PF=true; DF=true ;;
		"-dp") PF=true; DF=true ;;
	esac
	if [[ $ARG =~ [0-9]+ && $IDF = false ]]
	then
		ID=$ARG
		IDF=true
		echo $ID
	fi
done

if [ $IDF = false ] 
then 
	echo "Guessing…"
	ID=$(tail -n 1 $BASEPATH/.fax_history)
	if [ -z $ID ] 
	then
		echo "Nothing to guess from. Please send a fax first"
		exit 1
	fi
	IDF=true
fi

. $BASEPATH/authorization.sh # Checks if account works - after changed auth processes not necessarily needed anymore

JSON=$(curl -X GET -H  "accept: application/json"  --user $SIPUSER:$SIPPWD "https://api.sipgate.com/v2/history/$ID" -s)
DATE=$(echo $JSON | jq '.created' | sed -e 's:"::g' )
if [ -z $DATE ]
then
	echo "Fax seems to not exist with this account."
	exit 1
fi
DATE=$(date -d $DATE +%Y%m%d%H%M) 
FILE=$(echo $JSON | jq '.reportUrl' | sed -e 's:"::g' )
OUTFILE="$DATE-$ID-report.pdf"
if [ -z $FILE ]
then
	echo "Fax seems not to be sent successfully (yet). No report available"
	if [ $DF = true ]
	then
		read -p "Delete id from history anyways? (y/n) " DIN
		if [ $DIN =~ [yYjJ] ]
		then
			echo "$(grep -v "^$ID$" $BASEPATH/.fax_history)" > $BASEPATH/.fax_history
		fi
	fi
	exit 1
fi
if [ $PF = false ]
then
	echo "Speichere Report in $OUTFILE"
	curl $FILE --output $OUTFILE -s
else
	echo "Schicke Report an Drucker"
	curl $FILE -s --output - | lp -
fi
if [ $DF = true ]
then
	echo "$(grep -v "^$ID$" $BASEPATH/.fax_history)" > $BASEPATH/.fax_history
fi
