#! /bin/bash

function usage {
	echo "Usage: 
	$ ./fax.sh <letter.pdf> <recipient> 
	where <recipient> has to be a german faxline number
	$ ./fax.sh <letter.pdf> -handle <handle>
	Asks Letterhead's address database for <handle>'s fax number.
	$ ./fax.sh -help
	Print this Message and exit"
	exit 1 
}
function abort {
	echo "Letterhead API not available" 
	exit 1
}
BASEPATH=$(dirname $(realpath $0))
. $BASEPATH/.env

#Parsing Parameters:
unset $GETOPT_COMPATIBLE
ARGV=$(getopt -n "$0" -a -o "" -l "help,handle:" -- "$@")
if [ $? -ne 0 ]
then
	usage
fi
eval set -- "$ARGV"
REC0=
#Now we have $@ clean and tidy and begin parsing
while :
do
	case "$1" in
		"--help") usage ;;
		"--handle") 
			$LETTERHEAD_PATH/Adressen/address.sh -l > /dev/null || abort
			HAND=$2
			if [[ -e DECKBLATT.csv && $HAND = "+" ]]
			then
				HAND=$(grep + DECKBLATT.csv | cut -d, -f2)
			fi
			REC0=$($LETTERHEAD_PATH/Adressen/address.sh -f $HAND 2>/dev/null | sed 's: ::g')
			shift 
			shift ;;
		"--")
			shift
			break ;;
	esac
done
#Check if you have enough parameters
if [ -z $REC0 ]
then
	REC0=$2
fi
if [[ -z $REC0 || -z $1 ]]
then
	usage
fi
#Check if account works - not necessarily needed anymore, since sipgate changed their auth processes
. $BASEPATH/authorization.sh
#Prepare fax credentials:
FILE=$1
echo -n "Sending file ${FILE} "
REC=$(echo "$REC0" | sed -e 's:[^+0-9]::g' -e 's:^0\([1-9]\):+49\1:')
echo "to $REC"
#Checking for file size:
if [ ! -e "$FILE" ]
then
	echo "File not found"
	exit 2
fi
SIZE=$(stat -c%s "$FILE")
if [ $SIZE -gt 10485760 ]
then
	echo "File too big. Cannot be more than 10MB."
	exit 2
fi
if hash gs 2>/dev/null
then
	PAGES=$(gs -q -dNODISPLAY -dNOSAFER -c "($FILE) (r) file runpdfbegin pdfpagecount = quit" 2>&1)
	if [ $PAGES -gt 30 ]
	then
		echo "Sipgate won't send your file, because it has more than 30 pages"
		exit 2
	fi
else
	echo "ghostscript not installed, cannot check for page numbers. Sipgate accepts up to 30 pages."
fi
#Needs to be base64 encoded:
CONTENT=$(base64 "$FILE" -w 0)
if [ -z "$CONTENT" ];
then
	echo "Encoding failed"
	exit 2
fi
#save payload to tempfile to prevent curl from throwing 'argument list too long' error.
echo '{"faxlineId":"f0", "recipient":"'${REC}'", "filename":"fax.pdf", "base64Content":"'${CONTENT}'"}' > /tmp/sipgatedata.txt 
#Send fax
REPLY=$(curl --request POST --header 'Content-Type: application/json' --header 'Accept: application/json' --user $SIPUSER:$SIPPWD --data @/tmp/sipgatedata.txt "https://api.sipgate.com/v2/sessions/fax" -s)
FAXID=$(echo $REPLY | jq '.sessionId' | sed -e 's:"::g' )
echo $REPLY > /tmp/sipgatereply.txt
#remove tempfile
rm /tmp/sipgatedata.txt
if [ -z $FAXID ]
then
	echo "Something went wrong, I didn't receive some fax id back."
	exit 2
else
	echo "Sipgate took your fax as sessionID $FAXID"
	echo "$FAXID" >> $BASEPATH/.fax_history
fi
#Check if fax request was accepted by sipgate
curl --request GET --header 'Content-Type: application/json' --header 'Accept: application/json'  --user $SIPUSER:$SIPPWD "https://api.sipgate.com/v2/history/$FAXID" -s | jq '.faxStatusType'
#And show the account balance
. $BASEPATH/getbalance.sh
