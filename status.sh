#! /bin/bash

BASEPATH=$(dirname $(realpath $0))
. $BASEPATH/authorization.sh
while read LINE; 
do
	JSON=$(curl --request GET --header 'Content-Type: application/json' --header 'Accept: application/json'  --user $SIPUSER:$SIPPWD "https://api.sipgate.com/v2/history/$LINE" -s)
        STATUS=$(echo $JSON | jq '.faxStatusType' | sed -e 's:"::g' )
        DATE=$(echo $JSON | jq '.created' | sed -e 's:"::g' )
        TARGET=$(echo $JSON | jq '.target' | sed -e 's:"::g' )
	if [ -z $DATE ]
	then
		echo "Are you sure fax $LINE was sent from the logged in account? I cannot find it, so I'll skip it."
		continue
	fi
	echo "Fax $LINE vom $(date +%d.%m.%Y -d $DATE) an $TARGET: $STATUS"
done < $BASEPATH/.fax_history
echo "$(tail $BASEPATH/.fax_history)" > $BASEPATH/.fax_history
